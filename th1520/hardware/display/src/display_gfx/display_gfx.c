/*
 * Copyright (c) 2023 Thead CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h> 
#include "display_common.h"
#include "display_gralloc.h"
#include "securec.h"
#include "display_gfx.h"
#include "csi_g2d.h"
#include "csi_g2d_types.h"

#define G2D_DEBUG_EN 0
GrallocFuncs *grallocFucs = NULL;
unsigned int colorSpaceModeChange(PixelFormat color)
{
    unsigned int theadFormat;
    switch (color) {
        case PIXEL_FMT_RGB_565:          /**< RGB565 format */
            theadFormat = CSI_G2D_FMT_RGB565;
            break;
        case PIXEL_FMT_RGBX_8888:        /**< RGBX8888 format */
            theadFormat = CSI_G2D_FMT_RGBX8888;
            break;
        case PIXEL_FMT_RGBA_8888:        /**< RGBA8888 format */
            theadFormat = CSI_G2D_FMT_ABGR8888;
            break;
        case PIXEL_FMT_RGB_888:          /**< RGB888 format */
            theadFormat = CSI_G2D_FMT_RGB888;
            break;
        case PIXEL_FMT_BGR_565:          /**< BGR565 format */
            theadFormat = CSI_G2D_FMT_BGR565;
            break;
        case PIXEL_FMT_BGRX_8888:        /**< BGRX8888 format */
            theadFormat = CSI_G2D_FMT_BGRX8888;
            break;
        case PIXEL_FMT_BGRA_8888:        /**< BGRA8888 format */
            theadFormat = CSI_G2D_FMT_BGRA8888;
            break;
        case PIXEL_FMT_YCRCB_420_SP: {
            theadFormat = CSI_G2D_FMT_NV21;
            break;
        }
        case  PIXEL_FMT_YCBCR_420_SP: {
            theadFormat = CSI_G2D_FMT_NV12;
            break;
        }
        default: {
            DISPLAY_LOGE("colorSpaceModeChange default color: %{public}d", color);
            theadFormat = CSI_G2D_FMT_ABGR8888;  //CSI_G2D_FMT_ABGR8888
            break;
        }
    }

    return theadFormat;
}

int32_t blendTypeChange(BlendType blendType)
{
    int32_t theadBlendType;
    switch (blendType) {
        case BLEND_SRC:              /**< SRC blending */
            theadBlendType = CSI_G2D_BLEND_MODE_SRC;  //G2D_SRC_ALPHA
            break;
        case BLEND_DST:              /**< DST blending */
            theadBlendType = CSI_G2D_BLEND_MODE_DST;  //G2D_DST_ALPHA
            break;
        case BLEND_SRCOVER:          /**< SRC_OVER blending */
            theadBlendType = CSI_G2D_BLEND_MODE_SRC_OVER;  //G2D_ONE_MINUS_SRC_ALPHA
            break;
        case BLEND_DSTOVER:          /**< DST_OVER blending */
            theadBlendType = CSI_G2D_BLEND_MODE_DST_OVER;  //G2D_ONE_MINUS_DST_ALPHA
            break;
        default:
            theadBlendType = CSI_G2D_BLEND_MODE_SRC_OVER;
            break;
    }

    return theadBlendType;
}

int32_t TransformTypeChange(TransformType type)
{
    int32_t theadRotateType;
    switch (type) {
        case ROTATE_90:            /**< Rotation by 90 degrees */
            theadRotateType = CSI_G2D_ROTATION_90_DEGREE; //IM_HAL_TRANSFORM_ROT_90
            break;
        case ROTATE_180:             /**< Rotation by 180 degrees */
            theadRotateType = CSI_G2D_ROTATION_180_DEGREE; //IM_HAL_TRANSFORM_ROT_180
            break;
        case ROTATE_270:             /**< Rotation by 270 degrees */
            theadRotateType = CSI_G2D_ROTATION_270_DEGREE; //IM_HAL_TRANSFORM_ROT_270
            break;
        default:
            theadRotateType = CSI_G2D_ROTATION_0_DEGREE;        /**< No rotation */
            break;
    }
    return theadRotateType;
}
csi_g2d_surface target;
int have; 
int32_t THEADInitGfx()
{
    int ret;
    DISPLAY_DEBUGLOG("%{public}s", __func__);
    ret=csi_g2d_open();
    if (ret) {
        DISPLAY_LOGE("THEADInitGfx open csi g2d failed");
        return DISPLAY_FAILURE;
    }
    ret  = memset_s(&target, sizeof(target), 0x00, sizeof(target));
    if (ret) {
        DISPLAY_LOGE("memset_s failed");
    }
    target.width    = 800;
    target.height    = 1280;
    target.format    = CSI_G2D_FMT_ABGR8888;
    target.pool    = CSI_G2D_POOL_USER;

    ret = csi_g2d_surface_create(&target);
    if (ret) {
        DISPLAY_LOGE("create target surface failed");
        return ret;
    }
    have = 1;
    return DISPLAY_SUCCESS;
}

int32_t THEADDeinitGfx()
{
    int ret;
    DISPLAY_DEBUGLOG("%{public}s", __func__);
    ret = csi_g2d_surface_destroy(&target);
    if (ret) {
        DISPLAY_LOGE("destroy target surface failed ret: %{public}d", ret);
        return ret;
    }
    ret = csi_g2d_close();
    if (ret) {
        DISPLAY_LOGE("THEADDeinitGfx close g2d failed");
        return DISPLAY_FAILURE;
    }

    return DISPLAY_SUCCESS;
}

int32_t THEADSync(int32_t timeOut)
{
    DISPLAY_DEBUGLOG("%{public}s", __func__);
    return DISPLAY_SUCCESS;
}
static int g2d_load_bitmap(csi_g2d_surface *surface,int fd,int size)
{
    int i, j, lsize,ret;
    int width[3], height[3];
    void *ptr;
    void *ptrdst;

    if (!surface || !surface->nplanes || surface->nplanes > 3){
        DISPLAY_DEBUGLOG("g2d_load_bitmap error 1_1");
    return -1;
    }

    ptrdst = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (ptrdst == MAP_FAILED) {
        DISPLAY_LOGE("C9XX g2d_load_bitmap mmap failed");
        return 0;
    }
    
    width[0]  = surface->width;
    height[0] = surface->height;
    switch (surface->format) {
    case CSI_G2D_FMT_NV12:
    case CSI_G2D_FMT_NV21:
    width[1]  = surface->width / 2;
    height[1] = surface->height / 2;
    break;
    case CSI_G2D_FMT_ARGB8888:
    case CSI_G2D_FMT_BGRA8888:
    break;

    default:
    DISPLAY_DEBUGLOG("g2d_load_bitmap error 1_2");
    return -1;
    }

    for (i = 0; i < surface->nplanes; i++) {
    for (j = 0; j < height[i]; j++) {
        ptr = surface->lgcaddr[i] + j * surface->stride[i];
        lsize = width[i] * surface->cpp[i];
        if(memcpy_s(ptr, lsize, ptrdst, lsize) != 0){
            DISPLAY_LOGE("g2d_load_bitmap copy source buffer memcpy_s failed");
        }
        ptrdst = ptrdst + lsize;
    }
    }
    ret = munmap(ptrdst, size);
    if (ret != 0) {        
        DISPLAY_LOGE("C9XX g2d_load_bitmap %{public}s, Line:%{public}d ret: %{public}d", __func__,__LINE__, ret);
        return ret;
    }    

    return 0;
}
#if G2D_DEBUG_EN
static int g2d_save_bitmap(csi_g2d_surface *surface, const char *path)
{
    int i, j, bytes, lsize;
    int width[3], height[3];
    FILE *file;
    void *ptr;

    DISPLAY_DEBUGLOG("%{public}s", __func__);
    if (!surface || !surface->nplanes || surface->nplanes > 3)
    return -1;

    file = fopen(path, "w");
    if (!file)
    return -1;

    width[0]  = surface->width;
    height[0] = surface->height;
    switch (surface->format) {
    case CSI_G2D_FMT_NV12:
    case CSI_G2D_FMT_NV21:
    width[1]  = surface->width / 2;
    height[1] = surface->height / 2;
    break;
    /* TODO: add more formats support here */
    case CSI_G2D_FMT_ARGB8888:
    case CSI_G2D_FMT_RGBA8888:
    break;
    default:
    return -1;
    }

    for (i = 0; i < surface->nplanes; i++) {
    for (j = 0; j < height[i]; j++) {
        ptr = surface->lgcaddr[i] + j * surface->stride[i];
        lsize = width[i] * surface->cpp[i];

        bytes = fwrite(ptr, 1, lsize, file);
        if (bytes != lsize)
        return -1;
    }
    }
    fclose(file);
    return 0;
}
#endif

int32_t THEADFillRect(ISurface *iSurface, IRect *rect, uint32_t color, GfxOpt *opt)
{

    int ret;
    csi_g2d_surface target;
    csi_g2d_region dstRect;
    void *ptrdst_tar;
    int share_tarfd;
    size_t share_tarlength;
    DISPLAY_DEBUGLOG("%{public}s", __func__);


    ret  = memset_s(&target, sizeof(target), 0x00, sizeof(target));
    if (ret) {
        DISPLAY_LOGE("memset_s failed");
        return ret;
    }

    target.width    = iSurface->width;
    target.height    = iSurface->height;
    target.format    = colorSpaceModeChange(iSurface->enColorFmt);

    ret = csi_g2d_surface_create(&target);
    if (ret) {
        DISPLAY_LOGE("create target surface failed");
        return ret;
    }
    ret = csi_g2d_surface_set_target(&target);
    if (ret) {
        DISPLAY_LOGE("set target surface failed");
        return ret;
    }

    share_tarfd = (int)iSurface->phyAddr;
    share_tarlength =target.width *target.height* target.cpp[0];
    ptrdst_tar = mmap(NULL, share_tarlength, PROT_READ | PROT_WRITE, MAP_SHARED, share_tarfd, 0);
       if (ptrdst_tar == MAP_FAILED) {
        DISPLAY_LOGE("mmap failed");
        return DISPLAY_FAILURE;
    }
    
//    csi_g2d_surface_import_phy(&target,iSurface->phyAddr,ptrdst_tar);
    
    dstRect.left = rect->x;
    dstRect.top = rect->y;
    dstRect.right = (rect->x + rect->w);
    dstRect.bottom = (rect->y + rect->h);

    ret = csi_g2d_fill(&dstRect, 1, color);
    if (ret) {
        DISPLAY_LOGE("g2d fill operation failed");
        return ret;
    }

    ret = csi_g2d_flush();
    if (ret) {
        DISPLAY_LOGE("flush g2d failed");
        return ret;
    }

    ret = csi_g2d_surface_destroy(&target);
    if (ret) {
        DISPLAY_LOGE("destroy target surface failed");
        return ret;
    }
    ret = munmap(ptrdst_tar, share_tarlength);
    if (ret != 0) {
        DISPLAY_LOGE("munmap ptrdst_tar failed ret: %{public}d", ret);
    }

    DISPLAY_DEBUGLOG("THEADFillRect finished");
    return DISPLAY_SUCCESS;
}

int32_t THEADBlit(ISurface *srcSurface, IRect *srcRect, ISurface *dstSurface, IRect *dstRect, GfxOpt *opt)
{
    int ret;
    csi_g2d_surface source;
    csi_g2d_region dstRectThead,srcRectThead;
    csi_g2d_blend_mode blend_mode;
    csi_g2d_rotation rotation_mode;
    int test_size;
    test_size = srcSurface->width * srcSurface->height *3/2;

    if(have > 1){
    ret = csi_g2d_surface_destroy(&target);
        if (ret) {
            DISPLAY_LOGE("destroy source surface failed ret = %{public}d",ret);
            return ret;
        }
        ret  = memset_s(&target, sizeof(target), 0x00, sizeof(target));
        if (ret) {
            DISPLAY_LOGE("memset_s failed");
            return ret;
        }

        target.width    = dstSurface->width;
        target.height    = dstSurface->height;
        target.pool     = CSI_G2D_POOL_USER;
        target.format    = colorSpaceModeChange(dstSurface->enColorFmt);

        ret = csi_g2d_surface_create(&target);
        if (ret) {
            DISPLAY_LOGE("create target surface failed");
            return ret;
        }  
      
    }
    ret = memset_s(&source, sizeof(source), 0x00, sizeof(source));
    if (ret != 0) {
        DISPLAY_LOGE("%s: memset_s failed", __func__);
    }

    source.width    = srcSurface->width;
    source.height    = srcSurface->height;
    source.format    = colorSpaceModeChange(srcSurface->enColorFmt);
	source.stride[0]   =  srcSurface->stride;
    if(srcSurface->enColorFmt == PIXEL_FMT_YCBCR_420_SP) 
        source.pool     = CSI_G2D_POOL_DEFAULT;
    else
        source.pool     = CSI_G2D_POOL_USER;
    blend_mode = blendTypeChange(opt->blendType);
    rotation_mode = TransformTypeChange(opt->rotateType);    
    ret = csi_g2d_surface_create(&source);
    if (ret) {
        DISPLAY_LOGE("create source surface failed");
        return ret;
    }
    if(srcSurface->enColorFmt == PIXEL_FMT_YCBCR_420_SP) 
        ret = g2d_load_bitmap(&source,(int)srcSurface->phyAddr,test_size);
    else
        csi_g2d_surface_import(&source,(int)srcSurface->phyAddr); 
    ret = csi_g2d_surface_set_source(&source);
    if (ret) {
        DISPLAY_LOGE("set source surface failed");
        return ret;
    }

    csi_g2d_surface_import(&target,(int)dstSurface->phyAddr);
    ret = csi_g2d_surface_set_target(&target);
    if (ret) {
        DISPLAY_LOGE("set target surface failed");
        return ret;
    }
    srcRectThead.left = srcRect->x;;
    srcRectThead.top = srcRect->y;
    srcRectThead.right = (srcRect->x + srcRect->w);
    srcRectThead.bottom = (srcRect->y + srcRect->h);

    dstRectThead.left = dstRect->x;
    dstRectThead.top = dstRect->y;
    dstRectThead.right = (dstRect->x + dstRect->w);
    dstRectThead.bottom = (dstRect->y + dstRect->h);
    ret = csi_g2d_surface_set_source_alpha_mode(
    CSI_G2D_ALPHA_MODE_STRAIGHT,
    blend_mode
    );
    if (ret) {
    DISPLAY_LOGE("set source alpha mode failed\n");
    return ret;
    }
    
    ret = csi_g2d_surface_set_source_global_alpha_mode(
    CSI_G2D_GLOBAL_ALPHA_MODE_OFF,
    0xFFFFFFFF
    );
    if (ret) {
    DISPLAY_LOGE("set source global alpha mode failed\n");
    return ret;
    }

    ret = csi_g2d_surface_set_target_alpha_mode(
    CSI_G2D_ALPHA_MODE_STRAIGHT,
    blend_mode
    );
    if (ret) {
    DISPLAY_LOGE("set target alpha mode failed\n");
    return ret;
    }

    ret = csi_g2d_surface_set_target_global_alpha_mode(
    CSI_G2D_GLOBAL_ALPHA_MODE_OFF,
    0xFFFFFFFF
    );
    if (ret) {
    DISPLAY_LOGE("set target global alpha mode failed");
    return ret;
    }
    
    ret = csi_g2d_surface_enable_disable_alpha_blend(true);
    if (ret) {
    DISPLAY_LOGE("enable alpha blend failed\n");
    return ret;
    }
    
#if 0
    if (opt->enGlobalAlpha) {
        ret = csi_g2d_surface_set_target_alpha_mode(
        alpha_mode,
        blend_mode
        );
        if (ret) {
            DISPLAY_LOGE("set target alpha mode failed");
            return ret;
        }
        ret = csi_g2d_surface_enable_disable_alpha_blend(true);
        if (ret) {
            DISPLAY_LOGE("enable alpha blend failed");
            return ret;
        }
    } else {
        ret = csi_g2d_surface_enable_disable_alpha_blend(false);
        if (ret) {
            DISPLAY_LOGE("disable alpha blend failed");
            return ret;
        }
    }
#endif

#if 0//G2D_DEBUG_EN
    if (opt->enGlobalAlpha) {
        ret = csi_g2d_surface_set_target_global_alpha_mode(
        global_alpha_mode,
        global_color
        );
        if (ret) {
            DISPLAY_LOGE("set target global alpha mode failed");
            return ret;
        }
    }


    ret = csi_g2d_blit_set_rotation(rotation_mode);
    if (ret) {
    DISPLAY_LOGE("set rotation  failed");
    return ret;
    }
#endif

    ret = csi_g2d_blit_set_rotation(rotation_mode);
    if (ret) {
    DISPLAY_LOGE("set rotation  failed");
    return ret;
    }
    ret = csi_g2d_blit_set_filter_tap(CSI_G2D_FILTER_TAP_5,
            CSI_G2D_FILTER_TAP_5);
    if (ret) {
    DISPLAY_LOGE("set filter tap failed\n");
    return ret;
    }
    csi_g2d_surface_set_source_clipping(&srcRectThead);
    csi_g2d_surface_set_target_clipping(&dstRectThead);
    ret = csi_g2d_blit_filterblit(&dstRectThead, 1);
    if (ret) {
        DISPLAY_LOGE("csi_g2d_blit_filterblit failed  ret = %{public}d",ret);
        return ret;
    }

    ret = csi_g2d_flush();
    if (ret) {
        DISPLAY_LOGE("flush g2d failed");
        return ret;
    }

    ret = csi_g2d_surface_destroy(&source);
    if (ret) {
        DISPLAY_LOGE("destroy source surface failed ret = %{public}d",ret);
        return ret;
    }

    have = 2;
    return DISPLAY_SUCCESS;
}


int32_t GfxInitialize(GfxFuncs **funcs)
{

    DISPLAY_DEBUGLOG("%{public}s", __func__);
    DISPLAY_CHK_RETURN((funcs == NULL), DISPLAY_PARAM_ERR, DISPLAY_LOGE("info is null"));
    GfxFuncs *gfxFuncs = (GfxFuncs *)malloc(sizeof(GfxFuncs));
    DISPLAY_CHK_RETURN((gfxFuncs == NULL), DISPLAY_NULL_PTR, DISPLAY_LOGE("gfxFuncs is nullptr"));
    errno_t eok = memset_s((void *)gfxFuncs, sizeof(GfxFuncs), 0, sizeof(GfxFuncs));
    if (eok != EOK) {
        DISPLAY_LOGE("memset_s failed");
        free(gfxFuncs);
        return DISPLAY_FAILURE;
    }
    gfxFuncs->InitGfx = THEADInitGfx;
    gfxFuncs->DeinitGfx = THEADDeinitGfx;
    gfxFuncs->FillRect = THEADFillRect;
    gfxFuncs->Blit = THEADBlit;
    gfxFuncs->Sync = THEADSync;
    *funcs = gfxFuncs;
    DISPLAY_DEBUGLOG("GfxInitialize finish");
    return DISPLAY_SUCCESS;
}

int32_t GfxUninitialize(GfxFuncs *funcs)
{
    DISPLAY_DEBUGLOG("%{public}s", __func__);
    CHECK_NULLPOINTER_RETURN_VALUE(funcs, DISPLAY_NULL_PTR);
    free(funcs);
    return DISPLAY_SUCCESS;
}
